#!/bin/bash

NEW_VERSION=$1

# https://github.com/mikefarah/yq
APPLICATION_YAML_PATH="config/application.yml"
yq e -i ".spring.application.version = \"$NEW_VERSION\"" $APPLICATION_YAML_PATH
echo "'spring.application.version' in $APPLICATION_YAML_PATH updated with new version $NEW_VERSION"
