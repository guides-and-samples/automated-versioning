#!/bin/bash

SERVICE_NAME=$1
NEW_VERSION=$2

# https://github.com/mikefarah/yq
OPEN_API_SPECIFICATION_PATH="src/main/resources/open-api/specification/$SERVICE_NAME-openapi.yml"
yq e -i ".info.version = \"$NEW_VERSION\"" $OPEN_API_SPECIFICATION_PATH
echo "'info.version' in $OPEN_API_SPECIFICATION_PATH updated with new version $NEW_VERSION"
