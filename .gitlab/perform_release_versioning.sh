#!/bin/bash

# Required GitLab CI variables:
# CI_GIT_USER_EMAIL = email used by CI to commit changes, e.g. dev@payxpert.com
# CI_GIT_USER_USERNAME = user name used by CI to commit changes, e.g. GitLab CI
# SSH_PRIVATE_KEY = private SSH key that was used to create a deploy key at https://gitlab.payxpert.com/guides-and-samples/automated-versioning/-/settings/repository, e.g. ssh-rsa <SSH KEY BODY> <CI_GIT_USER_EMAIL>

apt update -y

# Install YQ
which wget || (apt install wget -y)
wget https://github.com/mikefarah/yq/releases/download/v4.14.2/yq_linux_amd64.tar.gz -O - | tar xz && mv yq_linux_amd64 /usr/bin/yq

# Install and set up git
which git || (apt install git -y)
git config --global user.email "$CI_GIT_USER_EMAIL"
git config --global user.name "$CI_GIT_USER_USERNAME"

# Install ssh-agent and set up known_hosts file (https://dev.to/nop33/git-commits-from-gitlab-ci-2l90)
which ssh-agent || (apt update -y && apt install openssh-client -y)
eval $(ssh-agent -s)
echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - >/dev/null
mkdir -p ~/.ssh
ssh-keyscan $CI_SERVER_HOST >>~/.ssh/known_hosts

SERVICE_NAME="test-application"
CURRENT_VERSION=$(.gitlab/get_current_version.sh)
SNAPSHOT_MARKER="-SNAPSHOT"
echo "Current version: [$CURRENT_VERSION] for service: [$SERVICE_NAME]"

if [[ $CURRENT_VERSION == *$SNAPSHOT_MARKER ]]; then
  echo "Automated versioning is taking the SNAPSHOT flow"
  NEW_RELEASE_VERSION=${CURRENT_VERSION%$SNAPSHOT_MARKER}

  if [[ $CI_COMMIT_REF_NAME != *$NEW_RELEASE_VERSION ]]; then
    echo "Skipping automated versioning process because new release version: [$NEW_RELEASE_VERSION] doesn't match the branch name: [$CI_COMMIT_REF_NAME]"
    exit
  fi

  echo "New release version: [$NEW_RELEASE_VERSION]"
  .gitlab/update_build_gradle_version.sh $NEW_RELEASE_VERSION &&
    .gitlab/update_application_yml_version.sh $NEW_RELEASE_VERSION &&
    .gitlab/update_open_api_version.sh $SERVICE_NAME $NEW_RELEASE_VERSION &&
    .gitlab/create_database_tag_change_set.sh $NEW_RELEASE_VERSION

  if [[ $(git status --porcelain) ]]; then
    git add "build.gradle.kts" "src/main/resources/open-api/specification/$SERVICE_NAME-openapi.yml" "config/application.yml" "src/main/resources/db/changelog/database-tag/db.changelog-database-tag-$NEW_RELEASE_VERSION.yml" "src/main/resources/db/changelog/db.changelog-master.yml" &&
      git commit -m "[👷 GitLab CI] 🔖 Update version from $CURRENT_VERSION to $NEW_RELEASE_VERSION [skip ci]" &&
      git push -o ci-skip git@$CI_SERVER_HOST:$CI_PROJECT_PATH.git HEAD:$CI_COMMIT_REF_NAME

  else
    echo "There is nothing to commit to the release branch, something went wrong"
    exit
  fi

  git fetch origin &&
    git checkout origin/develop &&
    git cherry-pick origin/$CI_COMMIT_REF_NAME

  NEW_SNAPSHOT_VERSION=$(.gitlab/increment_version.sh -m $NEW_RELEASE_VERSION)-SNAPSHOT
  echo "New SNAPSHOT version: [$NEW_SNAPSHOT_VERSION]"

  .gitlab/update_build_gradle_version.sh $NEW_SNAPSHOT_VERSION &&
    .gitlab/update_application_yml_version.sh $NEW_SNAPSHOT_VERSION &&
    .gitlab/update_open_api_version.sh $SERVICE_NAME $NEW_SNAPSHOT_VERSION

  if [[ $(git status --porcelain) ]]; then
    git add "build.gradle.kts" "src/main/resources/open-api/specification/$SERVICE_NAME-openapi.yml" "config/application.yml" &&
      git commit -m "[👷 GitLab CI] 📸 Update version from $NEW_RELEASE_VERSION to $NEW_SNAPSHOT_VERSION [skip ci]" &&
      git push -o ci-skip git@$CI_SERVER_HOST:$CI_PROJECT_PATH.git HEAD:develop

  else
    echo "There is nothing to commit to the develop branch, something went wrong"
    exit
  fi

else
  echo "Automated versioning is taking the release patch flow"
  NEW_RELEASE_VERSION=$(.gitlab/increment_version.sh -p $CURRENT_VERSION)

  if [[ $CI_COMMIT_REF_NAME != *$NEW_RELEASE_VERSION ]]; then
    echo "Skipping automated versioning process because new release version: [$NEW_RELEASE_VERSION] doesn't match the branch name: [$CI_COMMIT_REF_NAME]"
    exit
  fi

  echo "New release version: [$NEW_RELEASE_VERSION]"
  .gitlab/update_build_gradle_version.sh $NEW_RELEASE_VERSION &&
    .gitlab/update_application_yml_version.sh $NEW_RELEASE_VERSION &&
    .gitlab/update_open_api_version.sh $SERVICE_NAME $NEW_RELEASE_VERSION &&
    .gitlab/create_database_tag_change_set.sh $NEW_RELEASE_VERSION

  if [[ $(git status --porcelain) ]]; then
    git add "build.gradle.kts" "src/main/resources/open-api/specification/$SERVICE_NAME-openapi.yml" "config/application.yml" "src/main/resources/db/changelog/database-tag/db.changelog-database-tag-$NEW_RELEASE_VERSION.yml" "src/main/resources/db/changelog/db.changelog-master.yml" &&
      git commit -m "[👷 GitLab CI] 🔖 Update version from $CURRENT_VERSION to $NEW_RELEASE_VERSION [skip ci]" &&
      git push -o ci-skip git@$CI_SERVER_HOST:$CI_PROJECT_PATH.git HEAD:$CI_COMMIT_REF_NAME

  else
    echo "There is nothing to commit to the release branch, something went wrong"
    exit
  fi
fi
