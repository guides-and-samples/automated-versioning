#!/bin/bash

NEW_VERSION=$1

# https://stackoverflow.com/a/36627924/8765371
BUILD_GRADLE_PATH="build.gradle.kts"
sed -i "/^version = / s/=.*/= \"$NEW_VERSION\"/" $BUILD_GRADLE_PATH
echo "'version' property in $BUILD_GRADLE_PATH updated with new version $NEW_VERSION"
