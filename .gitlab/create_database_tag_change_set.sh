#!/bin/bash

NEW_VERSION=$1

CHANGE_LOG_DIRECTORY_PATH="src/main/resources/db/changelog"
DB_TAG_DIRECTORY_PATH="$CHANGE_LOG_DIRECTORY_PATH/database-tag"
if [ ! -d $DB_TAG_DIRECTORY_PATH ]; then
  mkdir $DB_TAG_DIRECTORY_PATH
fi

DB_TAG_CHANGE_SET_FILE_NAME="db.changelog-database-tag-$NEW_VERSION.yml"
DB_TAG_CHANGE_SET_PATH="$DB_TAG_DIRECTORY_PATH/$DB_TAG_CHANGE_SET_FILE_NAME"
echo "databaseChangeLog:
  - changeSet:
      id: tag-database-$NEW_VERSION
      author: Gitlab CI
      context: database-tag
      labels: new-version-$NEW_VERSION
      changes:
        - tagDatabase:
            tag: v$NEW_VERSION" >>$DB_TAG_CHANGE_SET_PATH
echo "Database tag change set $DB_TAG_CHANGE_SET_PATH for version $NEW_VERSION created"

MASTER_CHANGE_LOG_PATH="$CHANGE_LOG_DIRECTORY_PATH/db.changelog-master.yml"
echo "  - include:
      context: database-tag
      labels: new-version-$NEW_VERSION
      relativeToChangelogFile: true
      file: database-tag/$DB_TAG_CHANGE_SET_FILE_NAME" >>$MASTER_CHANGE_LOG_PATH
echo "Master change log $MASTER_CHANGE_LOG_PATH updated with new database tag change set"
